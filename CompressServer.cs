﻿using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;

namespace Server
{
    class Program
    {
        const int PORT_NO = 5000;
        const string SERVER_IP = "127.0.0.1";
        public static FileInfo gzipFileName;
        public static FileInfo fileToBeGZipped;
        //public static System.IO.FileStream OpenRead();

        //private static object ms;

        static void Main(string[] args)
        {
            MemoryStream ms = new MemoryStream();
            TcpListener listen = new TcpListener(3003);
            TcpClient client;
            int bufferSize = 1024;
            NetworkStream netStream;
            int bytesRead = 0;
            int allBytesRead = 0;

            // Start listening
            Console.WriteLine("Socket for server started!");
            listen.Start();

            // Accept client
            Console.WriteLine("Pairing with client...");
            client = listen.AcceptTcpClient();
            netStream = client.GetStream();
            Console.WriteLine("Paired!");

            // Read length of incoming data
            byte[] length = new byte[4];
            bytesRead = netStream.Read(length, 0, 4);
            int dataLength = BitConverter.ToInt32(length, 0);
            Console.WriteLine("File received with : " + length);

            // Read the data
            int bytesLeft = dataLength;
            byte[] data = new byte[dataLength];

            //decompress
            FileInfo fileToBeGZipped = new FileInfo(@"D:\\1\\bush.gif");
            FileInfo gzipFileName = new FileInfo(string.Concat(fileToBeGZipped.FullName, ".gz"));
            using (FileStream fileToDecompressAsStream = gzipFileName.OpenRead())
            {
                string decompressedFileName = @"D:\\1\\bush.gif";
                using (FileStream decompressedStream = File.Create(decompressedFileName))
                {
                    using (GZipStream decompressionStream = new GZipStream(fileToDecompressAsStream, CompressionMode.Decompress))
                    {
                        try
                        {
                            decompressionStream.CopyTo(decompressedStream);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }

                while (bytesLeft > 0)
                {

                    int nextPacketSize = (bytesLeft > bufferSize) ? bufferSize : bytesLeft;

                    bytesRead = netStream.Read(data, allBytesRead, nextPacketSize);
                    allBytesRead += bytesRead;
                    bytesLeft -= bytesRead;

                }

                Console.WriteLine("Server got the DATA!\nSaving to destination folder...");

                // Save image to folder
                //File.WriteAllBytes("D:\\2\\bush.gif", data);

                // Clean up
                netStream.Close();
                client.Close();
            }
        }
    }

}


