﻿using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        const int PORT_NO = 5000;
        const string SERVER_IP = "127.0.0.1";

        static void Main(string[] args)
        {
            IPAddress ipAddress = IPAddress.Parse("127.0.0.1");
            int port = 3003;
            int bufferSize = 1024;

            TcpClient client = new TcpClient();
            NetworkStream netStream;

            Console.WriteLine("Connecting to server...");

            // Connect to server
            try
            {
                client.Connect(new IPEndPoint(ipAddress, port));

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            netStream = client.GetStream();

            // Read bytes from image
            Console.WriteLine("Reading image...");
            byte[] data = File.ReadAllBytes("D:\\1\\bush.gif");

            // Build the package
            Console.WriteLine("Building the package for sending it to server...");
            byte[] dataLength = BitConverter.GetBytes(data.Length);
            byte[] package = new byte[4 + data.Length];
            dataLength.CopyTo(package, 0);
            data.CopyTo(package, 4);

            // Send to server
            Console.WriteLine("Sending to server...");
            int bytesSent = 0;
            int bytesLeft = package.Length;

            FileInfo fileToBeGZipped = new FileInfo(@"D:\\1\\bush.gif");
            FileInfo gzipFileName = new FileInfo(string.Concat(fileToBeGZipped.FullName, ".gz"));

            using (FileStream fileToBeZippedAsStream = fileToBeGZipped.OpenRead())
            {
                using (FileStream gzipTargetAsStream = gzipFileName.Create())
                {
                    using (GZipStream gzipStream = new GZipStream(gzipTargetAsStream, CompressionMode.Compress))
                    {
                        try
                        {
                            fileToBeZippedAsStream.CopyTo(gzipStream);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }

                while (bytesLeft > 0)
                {

                    int nextPacketSize = (bytesLeft > bufferSize) ? bufferSize : bytesLeft;

                    netStream.Write(package, bytesSent, nextPacketSize);
                    bytesSent += nextPacketSize;
                    bytesLeft -= nextPacketSize;

                }

               // File.WriteAllBytes("D:\\2\\bush.gif", data);

                // Clean up
                netStream.Close();
                client.Close();

            }
        }
    }
}
    

